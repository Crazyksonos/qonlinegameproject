// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameSession.h"
#include "ShooterGameSession.generated.h"


/**
 * 
 */
UCLASS()
class ONLINEGAME_API AShooterGameSession : public AGameSession
{
	GENERATED_BODY()
		virtual void RegisterServer() override;
};
